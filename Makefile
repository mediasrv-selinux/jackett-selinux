include /usr/share/selinux/devel/Makefile

define assign-jackett-ports
echo "Assigning Jackett port..."
semanage port -m -t jackett_port_t -p tcp 9117
endef

define remove-jackett-ports
echo "Removing Jackett port..."
semanage port -d -t jackett_port_t -p tcp 9117
endef

define relabel-jackett-files
echo "Relabeling files..."
restorecon -DR /var/lib/jackett/
restorecon -DR /opt/Jackett/
endef

define force-relabel-jackett-files
echo "Relabeling files..."
restorecon -R /var/lib/jackett/
restorecon -R /opt/Jackett/
endef

.PHONY: install uninstall update

install:
	semodule -v -i jackett.pp
	$(assign-jackett-ports)
	$(relabel-jackett-files)

uninstall:
	semodule -v -r jackett
	$(remove-jackett-ports)
	$(relabel-jackett-files)	

update:
	semodule -v -i jackett.pp
	$(force-relabel-jackett-files)

