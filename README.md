# jackett-selinux

Selinux policy module for jackett

#### Release notes

There may be conditions where .NET does not remove it's tracing pipes on shutdown. This
requires you to be running .NET (dotnet) and to have the lttng-ust library installed.

The policy has proper context for these files. However if they already exist it will not be set,
so checking for thier presence is recomended when first installing the policy.
Removing lttng-ust prevents their creation and is encouraged for production systems if the library
is not needed elsewhere (it should not be). Doing so removes a small risk of symlink attacks and
other potential issues.

The included systemd unit file sets RemoveIPC=yes which prevents removes these files on shutdown.